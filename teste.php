<?php

// <?php
header('Content-Type: text/html; charset=iso-8859-1');
// EXEMPLO 0 : CONEXÃO
// TESTAR O TRY CATCH FORÇANDO UM ERRO!!
// try{
//     $conexao = new PDO("mysql:host=localhost;dbname=curso", "root", "sophia");
//     $conexao = NULL;
//         }
//     catch (PDOException $e){
//         print "Ops, algo deu errado: " . $e->getMessage(). "<br>";
//     }

// EXEMPLO 1 : LISTAGEM NO FORMATO DE OBJETO
// LISTAGEM DAS INFORMAÇÕES DO BANCO DE DADOS EM FORMA DE OBJETOS
// try{
// $conexao = new PDO("mysql:host=localhost;dbname=curso", "root", "sophia");
// $resultados = $conexao->query("SELECT nome,email FROM curso_pessoa");
// var_dump($resultados); // Para mostrar o tipo impresso
// if($resultados) // Se a variavel $resultado estiver informações
// {
//     foreach ($resultados as $resultado)
//         {
//             /* Impressão dos dados das colunas empresa e contato da tabela
//              fornecedor. */
//             echo $resultado['nome']." | ". $resultado['email'] . "<br>";
//         }
//     }
    
//     $conexao = NULL; // Terminar a conexão
//     }
//     catch (PDOException $e){
//         print "Ops, algo deu errado: " . $e->getMessage(). "<br>";
//     }


    // EXEMPLO 2 : LISTAGEM NO FORMATO DE ARRAY

    $conexao = new PDO("mysql:host=localhost;dbname=curso", "root", "sophia");
try{
    $resultados = $conexao->query("SELEC nome,email FROM curso_pessoa");
    $pessoas = $resultados->fetchAll();
    echo "<pre>";
        var_dump($pessoas);
    //     var_dump($pessoas[0]['nome']);
    //     var_dump($pessoas[0]['email']);
    //     var_dump($pessoas[0][0]);
    echo "</pre>";
    return true;
    
    
}
catch (PDOStatement $e){
    print "Ops, algo deu errado: " . $e->getMessage(). "<br>";
    return false;
}



// EXEMPLO 3 : LISTAGEM NO FORMATO DE ARRAY COM MANIPULAÇÃO DO FOREACH

// try{
//     $conexao = new PDO("mysql:host=localhost;dbname=curso", "root", "sophia");
//     $resultados = $conexao->query("SELECT nome,email FROM curso_pessoa");
//     $pessoas = $resultados->fetchAll();

//     foreach($pessoas as $pessoa){
//         var_dump($pessoa['nome']." => ".$pessoa['email'].'<br>');
//     }
//     $conexao = NULL;
// }
// catch (PDOException $e){
//     print "Ops, algo deu errado: " . $e->getMessage(). "<br>";
// }


// EXEMPLO 4: USANDO O METODO fetch() E OS MÉTODOS

// TRY{
// $conexao = new PDO("mysql:host=localhost;dbname=curso", "root", "sophia");
// $resultados = $conexao->query("SELECT nome,email FROM curso_pessoa");
// // $pessoas = $resultados->fetch(); // Traz apenas a primeira linha da coluna ordenado ordenadas por keys associativos numericos e string.
// // $pessoas = $resultados->fetch(PDO::FETCH_ASSOC); // Traz apenas a primeira linha da coluna com as keys associativos ordenados por string.
// // $pessoas = $resultados->fetch(PDO::FETCH_BOTH); // Traz apenas a primeira linha da coluna com as keys ordenado por nome das tabelas e numero de posiçao.
// // $pessoas = $resultados->fetch(PDO::FETCH_LAZY); // Traz apenas a primeira linha da coluna com as keys ordenado por string e posições numericas das tabelas e uma key com o query usado na busca.
// // $pessoas = $resultados->fetch(PDO::FETCH_NUM); // Traz apenas a primeira linha da coluna com as keys ordenadas pela posição numero.
// echo "<pre>";
// var_dump($pessoas);

// echo "</pre>";
// $conexao = NULL;
// }
// catch (PDOException $e){
//     print "Ops, algo deu errado: " . $e->getMessage(). "<br>";
// }

// EXEMPLO 5: INSERINDO DADOS com ?

//     try{
//         $conexao = new PDO("mysql:host=localhost;dbname=curso", "root", "sophia");
//         $sql = $conexao->prepare("INSERT INTO curso_pessoa (id,nome,email,celular,dataNascimento) VALUES(?,?,?,?,?)");

//         $sql->bindValue(1, 30);
//         $sql->bindValue(2, 'Fabricio');
//         $sql->bindValue(3, 'fabfab@gmail.com');
//         $sql->bindValue(4, '(67)87234567');
//         $sql->bindValue(5, '2011-02-04');

//         $sql->execute();
//         $conexao = NULL;
//     }
//     catch (PDOException $e){
//         print "Ops, algo deu errado: " . $e->getMessage(). "<br>";
//     }

// EXEMPLO 6: INSERINDO DADOS COM :CHAVES

//     try{
//         $conexao = new PDO("mysql:host=localhost;dbname=curso", "root", "sophia");
//         $sql = $conexao->prepare("INSERT INTO curso_pessoa (id,nome,email,celular,dataNascimento) VALUES(:id,:nome,:email,:celular,:dataNascimento)");

//         $sql->bindValue(':id', 31);
//         $sql->bindValue(':nome', 'Oracio');
//         $sql->bindValue(':email', 'orab@hotmail.com');
//         $sql->bindValue(':celular', '(67)8746537');
//         $sql->bindValue(':dataNascimento', '2013-06-08');

//         $sql->execute();
//         $conexao = NULL;
//     }
//     catch (PDOException $e){
//         print "Ops, algo deu errado: " . $e->getMessage(). "<br>";
//         die();
//     }


// EXEMPLO 7: ATUALIZAND DADOS COM :CHAVES

//     try{
//         $conexao = new PDO("mysql:host=localhost;dbname=curso", "root", "sophia");
//         $sql = $conexao->prepare("UPDATE curso_pessoa SET email = :email, celular = :celular WHERE id = :id");


//         $sql->bindValue(':id', 31);
//         $sql->bindValue(':email', 'orab500@hotmail.com');
//         $sql->bindValue(':celular', '(67)111111');

//         $sql->execute();
//         $conexao = NULL;
//     }
//     catch (PDOException $e){
//         print "Ops, algo deu errado: " . $e->getMessage(). "<br>";
//         die();
//     }


// EXEMPLO 9: ATUALIZAND DADOS COM ?

//     try{
//         $conexao = new PDO("mysql:host=localhost;dbname=curso", "root", "sophia");
//         $sql = $conexao->prepare("UPDATE curso_pessoa SET email = ?, celular = ? WHERE id = ?");


//         $sql->bindValue(3, 31);
//         $sql->bindValue(1, 'orab500@yahoo.com.br');
//         $sql->bindValue(2, '(67)11666');

//         $sql->execute();
//         $conexao = NULL;
//     }
//     catch (PDOException $e){
//         print "Ops, algo deu errado: " . $e->getMessage(). "<br>";
//         die();
//     }

// EXEMPLO 10: ATUALIZAND DADOS COM :CHAVES E ARRAY NO EXECUTE

//     try{
//         $conexao = new PDO("mysql:host=localhost;dbname=curso", "root", "sophia");
//         $sql = $conexao->prepare("UPDATE curso_pessoa SET email = :email, celular = :celular WHERE id = :id");

//         $sql->execute(array(':id'=>31,':email'=>'orab500@outlook.com',':celular'=>'(67)222222'));
//         $conexao = NULL;
//     }
//     catch (PDOException $e){
//         print "Ops, algo deu errado: " . $e->getMessage(). "<br>";
//         die();
//     }


// EXEMPLO 11: DELETANDO DADOS

//     try{
//         $conexao = new PDO("mysql:host=localhost;dbname=curso", "root", "sophia");
//         $sql = $conexao->prepare("DELETE FROM curso_pessoa WHERE id = ?");

//         $sql->bindValue(1,31);
//         $sql->execute();
//         $conexao = NULL;
//     }
//     catch (PDOException $e){
//         print "Ops, algo deu errado: " . $e->getMessage(). "<br>";
//         die();
//     }

?>
  

?>