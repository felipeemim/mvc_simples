<!DOCTYPE html>
<html lang="pt-br">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
     <title>Fundamentos básicos com PHP</title>
    <link rel="stylesheet" href="./style/style.css" media="all" />

</head>
<body style="background-color: #0088ff;">
    <Div style="position: absolute; bottom: 15%; left: 40%; background-color: white;padding: 15px 15px 15px 15px">
        
         <h2 style='text-align: center'>Cadastre-se</h2>
    <form method="get"  class="form">
    <p>
        <label for="nome">Nome</label>
        <input name="nome" type="text" placeholder="Seu Nome" />
    </p>
    
    <p>
        <label for="email">E-mail</label>
        <input name="email" type="text" placeholder="mail@exemplo.com.br" />
    </p>
    
    <p>
        <label for="login">Login</label>
        <input name="login" type="text" placeholder="Seu login" />
    </p>

	<p>
        <label for="senha">Senha</label>
        <input name="senha" type="text" placeholder="Sua senha" />
    </p>
    

    <p class="submit">
        <input name = 'submit' type="submit" value="Cadastrar" />
    </p>
    </form>
    <?php 
    include_once '../controller/ControllerSession.php';
    include_once '../controller/ControllerFormulario.php';
    $controllerFormulario = new ControllerFormulario();
    $session = new Session();
    $session->start();
    $controllerFormulario->executarLogin($session);
    
    ?>
    </Div>
</body>
</html>